
import math
import numpy as np
from numpy import s_

gen = np.random.default_rng(seed=smilei_mpi_rank)

'''
constants
'''
c_SI = 299792458.0
wavelength_SI = 0.8E-6

pi = math.pi
wavelength = 2.0 * pi
T0 = 2.0 * pi

femto = T0 / (wavelength_SI / c_SI / 1E-15)
um = 1E-6 / wavelength_SI * wavelength
hbar = 0.057 / 137**2

N = 20
T = N*T0

w0 = 10*um
a0 = 2

'''
mirror parameters

`nstairs` is the number of HPM pairs

`f` is the focal length
'''
# nstairs = 15
n0 = 100.0
f = f*um

# distance between the bunch and the laser center
Le = 60*um

bunch_start = N/2*T0 + Le
bunch_length = 2*um

# distance between HPM pairs
offset = 5*um

thickness = 7*um

dtheta = pi / nstairs

rmin = 1*um
rmax = 20*um

# first focal position to the left boundary
spacing = 10*um
# position of the first mirror
loc_first = spacing + f

'''geometry'''
Lx = np.ceil((loc_first + (nstairs-1)*offset + thickness)/wavelength) * wavelength
Ly = 165*wavelength / 3
Lz = 165*wavelength / 3

nx = int(Lx/wavelength)*20
ny = 960 / 3
nz = 960 / 3

dx = Lx / nx
dy = Ly / ny
dz = Lz / nz

'''
mirror density
'''
def mirror_dens(x, y, z):
    n = np.zeros_like(x)
    y -= Ly/2
    z -= Lz/2

    theta = np.arctan2(y, z)
    theta = np.where( theta <=0, theta + pi, theta)

    r = np.sqrt(y**2 + z**2)

    theta_idx = np.floor(theta / dtheta)
    steps = theta_idx * offset

    surf = loc_first - 1/(4*f) * r**2 + steps

    n = np.where( (x >= surf) & (x <= surf + thickness), n0, n)
    n = np.where( (r < rmin) | (r > rmax), 0.0, n)
    
    if chopped:
        '''chopped'''
        n = np.where( x < loc_first, 0, n)
    return n


'''
electron bunch
'''
def bunch_dens(n0_, r):
    def bunch_dens_(x, y, z):
        if(np.sqrt((y - Ly/2)**2 + (z - Lz/2)**2) < r):
            return n0_
        return 0
    return bunch_dens_


'momentum distribution of the bunch'
gamma = 500 / 0.51
collimation = 0.001 # FWHM

def px(x, y, z):
    g = gen.standard_normal()*0.01 * gamma + gamma
    return g


def py(x, y, z):
    g = gen.standard_normal()*collimation/2.355 * gamma
    return g


def pz(x, y, z):
    g = gen.standard_normal()*collimation/2.355 * gamma
    return g


'bunch profile for injection'
def bunch_prof(t):
    if(t >= bunch_start and t <= bunch_start + bunch_length):
        return 1
    else:
        return 0


'laser profile'
def prof(t):
    if( t <= T):
        return np.sin(t/T * np.pi)
    else:
        return 0.0


'''
simulation settings
'''
Main(
    geometry = "3Dcartesian",

    interpolation_order = 2,

    timestep_over_CFL = 0.65,
    simulation_time = bunch_start + spacing + (nstairs-1)*offset + 5*um, 

    cell_length  = [dx, dy, dz],
    grid_length =  [Lx, Ly, Lz],
    number_of_patches = [1, 16, 16],
    clrw = nx,

    EM_boundary_conditions = 
    [ 
        ["silver-muller", "silver-muller"],
        ["silver-muller", "silver-muller"],
        ["silver-muller", "silver-muller"]
    ],

    solve_poisson = False,
    
    solve_relativistic_poisson = False,

    reference_angular_frequency_SI = 2*pi*c_SI/wavelength_SI,
   
    print_every = 100,

    random_seed = smilei_mpi_rank
)

RadiationReaction(
  minimum_chi_continuous = 1e-4,
  minimum_chi_discontinuous = 1e-2
)

dt = Main.timestep

LaserGaussian3D(
    box_side         = "xmin",
    a0               = a0,
    omega            = 1.,
    focus            = [0, Ly/2, Lz/2],
    waist            = w0,
    incidence_angle  = [0, 0],
    polarization_phi = 0.,
    ellipticity      = 0.,
    time_envelope    = prof
)

Species(
    name = "electron",
    position_initialization = "regular",
    momentum_initialization = "cold",
    number_density = mirror_dens,
    particles_per_cell = 8,
    momentum_profile = [0, 0, 0],
    temperature = [0],
    mass = 1.0,
    charge = -1.0,
    pusher = "boris",
    boundary_conditions = [
       ["remove", "remove"],
       ["remove", "remove"],
       ["remove", "remove"]
    ],
)

Species(
    name = "proton",
    position_initialization = "regular",
    momentum_initialization = "cold",
    number_density = mirror_dens,
    particles_per_cell = 8,
    momentum_profile = [0, 0, 0],
    mass = 1836,
    charge = 1.0,
    pusher = "boris",
    boundary_conditions = [
       ["remove", "remove"],
       ["remove", "remove"],
       ["remove", "remove"]
    ],
)


Species(
    name = "bunch1",
    position_initialization = "random",
    momentum_initialization = "cold",
    particles_per_cell = 0,
    number_density = 0,
    momentum_profile = [px, py, pz],
    temperature = [0],
    mass = 1.0,
    charge = -1.0,
    pusher = "boris",
    boundary_conditions = [
       ["remove", "remove"],
       ["remove", "remove"],
       ["remove", "remove"]
    ],
    radiation_model = "ll",
)

ParticleInjector(
    species   = "bunch1",
    box_side  = "xmin",
    particles_per_cell = 10,
    number_density = bunch_dens(1E-2, 0.5*um),
    temperature = [0],
    time_envelope = bunch_prof
)

Species(
    name = "probe1",
    position_initialization = "random",
    momentum_initialization = "cold",
    particles_per_cell = 0,
    number_density = 0,
    momentum_profile = [px, py, pz],
    mass = 1.0,
    charge = -1.0,
    pusher = "boris",
    boundary_conditions = [
       ["remove", "remove"],
       ["remove", "remove"],
       ["remove", "remove"]
    ],
    radiation_model = "ll",
    is_test = True
)
ParticleInjector(
    species   = "probe1",
    box_side  = "xmin",
    particles_per_cell = 1,
    number_density = bunch_dens(1E-2, 1*um),
    temperature = [0],
    time_envelope = bunch_prof
)

'on-axis probe particles'
def probe2_dens():
    nprobe = 1000
    x = np.linspace(0, bunch_length*2, nprobe)
    y = np.zeros_like(x, dtype=float) + Ly/2
    z = np.zeros_like(x, dtype=float) + Lz/2
    w = np.zeros_like(x, dtype=float)
    part = np.concatenate((x.reshape([1,nprobe]), 
                           y.reshape([1,nprobe]), 
                           z.reshape([1,nprobe]), 
                           w.reshape([1,nprobe])), axis=0)
    return part

Species(
    name = "probe2",
    position_initialization = probe2_dens(),
    momentum_initialization = "cold",
    momentum_profile = [gamma, 0, 0],
    time_frozen = bunch_start + bunch_length*2,
    mass = 1.0,
    charge = -1.0,
    pusher = "boris",
    boundary_conditions = [
       ["remove"],
    ],
    radiation_model = "ll",
    is_test = True
)

DiagProbe(
    every    = 30,
    origin   = [0, Ly/2, Lz/2],
    vectors  = [
        [Lx, 0, 0]
        ],
    number   = [10000],
    fields   = ["Ex", "Ey", "Ez"]
)

'scalars'
DiagScalar(
    every = 30,
    vars = ["Urad_bunch1", "Ukin_bunch1", "EyMax"],
)

DiagTrackParticles(
    species = "bunch1",
    every = [int((bunch_start+2*bunch_length)/dt), int((bunch_start+loc_first+nstairs*offset)/dt), 100],
    attributes = ["x", "y", "z", "px", "py", "pz"]
)
DiagTrackParticles(
    species = "probe1",
    every = [int((bunch_start+2*bunch_length)/dt), int((bunch_start+loc_first+nstairs*offset)/dt), 1],
    attributes = [
        "x", "y", "z", 
        "px", "py", "pz",
        "Ex", "Ey", "Ez", 
        "Bx", "By", "Bz",
        "chi"
    ]
)
DiagTrackParticles(
    species = "probe2",
    every = [int((bunch_start+2*bunch_length)/dt), int((bunch_start+loc_first+nstairs*offset)/dt), 1],
    attributes = [
        "x", "y", "z", 
        "px", "py", "pz",
        "Ex", "Ey", "Ez", 
        "Bx", "By", "Bz",
        "chi"
    ]
)
